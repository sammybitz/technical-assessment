        var messages = {
    
            "request":[
                {
                     "msg": "INITIALIZE",
                     "body":null
                     
                },
            
            
                {      
                    "msg": "NODE_CLICKED",
                    "body": {
                        "x": 0|1|2|3,
                        "y": 0|1|2|3
                }
                }
            ],
            "response":[
                {
                "msg": "START",
            "body": {
                "newLine": null,
                "heading": "Player 1",
                "message": "Awaiting Player 1's Move"
            }
        }  ,
                {
                    "msg": "VALID_START_NODE",
                    "body": {
                        "newLine": null,
                        "heading": "Player 1"|| "Player 2",
                        "message": "Select a second node to complete the line."
                    }
                },
                {
                    "msg": "INVALID_START_NODE",
                    "body": {
                        "newLine": null,
                        "heading": "Player 1"||"Player 2",
                        "message": "Not a valid starting position."
                    }
                },
                {
                    "msg": "VALID_END_NODE",
                    "body": {
                        "newLine": {
                            "start": {
                                "x": 0|1|2|3,
                                "y": 0|1|2|3
                            },
                            "end": {
                                "x": 0|1|2|3,
                                "y": 0|1|2|3
                            }
                        },
                        "heading": "Player 1"|| "Player 2",
                        "message": null
                    }
                },
                {
                    "msg": "INVALID_END_NODE",
                    "body": {
                        "newLine": null,
                        "heading": "Player 1"||"Player 2",
                        "message": "Invalid move!"
                    }
                },
                {
                    "msg": "GAME_OVER",
                    "body": {
                        "newLine": {
                            "start": {
                                "x": 0|1|2|3,
                                "y": 0|1|2|3
                            },
                            "end": {
                                "x": 0|1|2|3,
                                "y": 0|1|2|3
                            }
                        },
                        "heading": "Game Over",
                        "message": "Player 1 Wins!"||"Player 2 Wins!"
                    }
                },
                {
                    "msg": "ERROR",
                    "body": "Invalid type for `id`: Expected INT but got a STRING"
                }

                
            ]
            }

function startGame()
{
 
    var playerNo = 1;
    var badNodes = [];
   var badXs = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5];
   var badYs = [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5];
   var count = 0;
    var turnID = 1;
    var properEndX = 0;
    var properEndY = 0;
    var initialStartX = 0;
    var initialStartY = 0;
    var initialEndX = 0;
    var initialEndY = 0;
    
app.ports.request.subscribe( (message) => {
    var violations = 0;
    
    message = JSON.parse(message);
    
   
   var verdad = false;
    console.log(message);
    console.log("TURN ID: " + turnID);
    
    
    //print(message);
    // Parse the message to determine a response, then respond:
    //app.heading = "Player 1";
    //app.message = "Awaiting Player 1's move";
    //console.log(newMsg);
        
        if (message.msg == "INITIALIZE")
        {
        newMsg = messages['response'][0]['msg'];
    
    newBody = messages['response'][0]['body'];
    
        }
        else
        {
            console.log("[")
            for (var i = 0; i < count; i++)
        {
            console.log(badXs[i] + ",");
            console.log(badYs[i] + ",");
            if (message.body.x == badXs[i] && message.body.y == badYs[i] && verdad == false)
            {
                newMsg = "INVALID_START_NODE";
                newBody = messages['response'][2]['body'];
                verdad = true;
            }
            else
            {
                
            }
        }
        console.log("]");
        if (turnID > 1)
        {
            console.log("VALID ENDING X: " + properEndX);
                console.log("VALID ENDING Y: " + properEndY);
                
                
        }
        console.log(verdad);
        
        for (var x = 0; x < badXs.length; x++)
        {
            if (initialStartX-1 == badXs[x] && initialStartY == badYs[x])
            {
                console.log("True");
                violations++;
            }
            if (initialStartX+1 == badXs[x] && initialStartY == badYs[x])
            {
                console.log("True2");
                violations++;
            }
            if (initialEndX-1 == badXs[x] && initialEndY == badYs[x])
            {
                console.log("True3");
                violations++;
            }
            if (initialEndX+1 == badXs[x] && initialEndY == badYs[x])
            {
                console.log("True4");
                violations++;
            }
            if (initialStartX == badXs[x] && initialStartY-1 == badYs[x])
            {
                console.log("True5");
                violations++;
            }
            if (initialStartX == badXs[x] && initialStartY+1 == badYs[x])
            {
                console.log("True6");
                violations++;
            }
            if (initialEndX == badXs[x] && initialEndY-1 == badYs[x])
            {
                console.log("True7");
                violations++;
            }
            if (initialEndX == badXs[x] && initialEndY+1 == badYs[x])
            {
                console.log("True8");
                violations++;
            }
            if (initialStartX+1 == badXs[x] && initialStartY+1 == badYs[x])
            {
                console.log("True9");
                violations++;
            }
            if (initialEndX+1 == badXs[x] && initialEndY+1 == badYs[x])
            {
                console.log("True10");
                violations++;
            }
            if (initialStartX+1 == badXs[x] && initialStartY-1 == badYs[x])
            {
                console.log("True11");
                violations++;
            }
            if (initialEndX+1 == badXs[x] && initialEndY-1 == badYs[x])
            {
                console.log("True12");
                violations++;
            }
            if (initialStartX-1 == badXs[x] && initialStartY+1 == badYs[x])
            {
                console.log("True13");
                violations++;
            }
            if (initialEndX-1 == badXs[x] && initialEndY+1 == badYs[x])
            {
                console.log("True14");
                violations++;
            }
            if (initialStartX-1 == badXs[x] && initialStartY-1 == badYs[x])
            {
                console.log("True15");
                violations++;
            }
            if (initialEndX-1 == badXs[x] && initialEndY-1 == badYs[x])
            {
                console.log("True16");
                violations++;
            }
        }
        
        console.log("TOTAL NUMBER OF VIOLATIONS: " + violations);
        if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 0 || initialEndY == 3) && violations >= 6)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 0 || initialEndY == 3) && violations >= 8)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 1 || initialEndY == 2) && violations >= 8)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 0 || initialEndY == 3) && violations >= 8)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 0 || initialEndY == 3) && violations >= 8)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 1 || initialEndY == 2) && violations >= 10)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 0 || initialEndY == 3) && violations >= 10)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 0 || initialEndY == 3) && violations >= 10)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 1 || initialEndY == 2) && violations >= 10)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 1 || initialEndY == 2) && violations >= 11)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 0 || initialEndY == 3) && violations >= 11)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 0 || initialEndX == 3) && (initialEndY == 1 || initialEndY == 2) && violations >= 13)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 0 || initialEndY == 3) && violations >= 13)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 0|| initialStartX == 3) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 1 || initialEndY == 2) && violations >= 13)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 0|| initialStartY == 3) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 1 || initialEndY == 2) && violations >= 13)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        else if ((initialStartX == 1|| initialStartX == 2) && (initialStartY == 1|| initialStartY == 2) && (initialEndX == 1 || initialEndX == 2) && (initialEndY == 1 || initialEndY == 2) && violations >= 16)
        {
            newMsg = messages['response'][5]['msg'];
            newBody = messages['response'][5]['body'];
            if (playerNo-1 % 2 == 0)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
            console.error("GAME OVER");
        }
        if (verdad == true && newMsg != "GAME_OVER")
        {
            newMsg = messages['response'][2]['msg'];
            newBody = messages['response'][2]['body'];
            console.log("NEW MESSAGE: " + newMsg);
        }
        console.log("Curent Player No. " + playerNo);
        if (turnID %2 == 0)
        {
            
            message.heading = "Player 2";
            newMsg.heading = "Player 2";
        }
        else{
            message.heading = "Player 1";
            newMsg.heading = "Player 1";
        }
        
        if (newMsg == "VALID_START_NODE")
        {
            for (var a = 0; a < badXs; a++)
            {
                if(badXs[a] == tempX && badYs[a] == tempY)
                {
                    
                    newMsg = messages['response'][2]['msg'];
                    newBody = messages['response'][2]['body'];
                    console.log("MADDY");
                }
                else{
                    
                }
                
            }
            if (newMsg != messages['response'][2]['msg'])
            {
                
            if (message.body.x == tempX && message.body.y != tempY)
            {
 
                console.log("Temporary X" + tempX);
                console.log("Temporary Y: " + tempY);
                
                for (var i = 0; i < badXs.length; i++)
                {
                    //console.log("foo");
                    if(verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if(verdad != true && message.body.x == badXs[i] && message.body.y < badYs[i] && tempY > badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if(verdad != true && message.body.x == badXs[i] && message.body.y > badYs[i] && tempY < badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
                    newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            if (message.body.y == tempY+2)
            {
                badXs[count] = tempX;
                badYs[count] = tempY+1;
                count++;
            }
            else if (message.body.y == tempY+3)
            {
                badXs[count] = tempX;
                badYs[count] = tempY+1;
                count++;
                badXs[count] = tempX;
                badYs[count] = tempY+2;
                count++;

            }
            else if (message.body.y == tempY-2)
            {
                badXs[count] = tempX;
                badYs[count] = tempY-1;
                count++;
            }
            else if (message.body.y == tempY-3)
            {
                badXs[count] = tempX;
                badYs[count] = tempY-1;
                count++;
                badXs[count] = tempX;
                badYs[count] = tempY-2;
                count++;
            }

         //   newBody.heading = "Player " + (playerNo+1%2);
            if (turnID > 1)
            {
                console.log("MOAT");
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
                }
            
            }
            else if (message.body.x != tempX && message.body.y == tempY)
            {
                console.log("Temporary X" + tempX);
                console.log("Temporary Y: " + tempY);
                console.log("NOW TESTING UP-DOWN");
                for (var i = 0; i < badYs.length; i++)
                {
                    console.log("foo");
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if(verdad != true && message.body.x < badXs[i] && message.body.y == badYs[i] && tempX > badXs[i])
                    {

                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if(verdad != true && message.body.x > badXs[i] && message.body.y == badYs[i] && tempX < badXs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
                    newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
            if (message.body.x == tempX+2)
            {
                badXs[count] = tempX+1;
                badYs[count] = tempY;
                count++;
            }
            else if (message.body.x == tempX+3)
            {
                badXs[count] = tempX+1;
                badYs[count] = tempY;
                count++;
                badXs[count] = tempX+2;
                badYs[count] = tempY;
                count++;

            }
            else if (message.body.x == tempX-2)
            {
                badXs[count] = tempX-1;
                badYs[count] = tempY;
                count++;
            }
            else if (message.body.x == tempX-3)
            {
                badXs[count] = tempX-1;
                badYs[count] = tempY
                count++;
                badXs[count] = tempX-2;
                badYs[count] = tempY;
                count++;
            }
            if (turnID > 1)
            {
                console.log("MOAT");
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }

                }
            

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+1 && message.body.y == tempY+1)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
                    newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }

                }
            

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+2 && message.body.y == tempY+2)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]+1 && message.body.y == badYs[i]+1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
             if (verdad == false)
             {
                newMsg = messages['response'][3]['msg'];
                newBody = messages['response'][3]['body'];
                newBody.newLine.start.x = tempX;
                newBody.newLine.start.y = tempY;
                
                newBody.newLine.end.x = message.body.x;
                newBody.newLine.end.y = message.body.y;
                console.log(newBody.newLine.end.x);
                console.log(newBody.newLine.end.y);
                badXs[count] = tempX+1;
                badYs[count] = tempY+1;
                count++;
                if (turnID > 1)
                {
                    badXs[count] = tempX;
                    badYs[count] = tempY;
                    count++;
                }
                fooX = newBody.newLine.end.x;
                fooY = newBody.newLine.end.y;
                

             }
            

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+3 && message.body.y == tempY+3)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]+1 && message.body.y == badYs[i]+1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]+2 && message.body.y == badYs[i]+2)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
            if (verdad == false)
            {
                newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX+1;
            badYs[count] = tempY+1;
            count++;
            badXs[count] = tempX+2;
            badYs[count] = tempY+2;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;

            }
            

         //  newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+1 && message.body.y == tempY-1)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                   
                    
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {   
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+2 && message.body.y == tempY-2)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]+1 && message.body.y == badYs[i]-1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX+1;
            badYs[count] = tempY-1;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX+3 && message.body.y == tempY-3)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]+1 && message.body.y == badYs[i]-1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    if (verdad != true && message.body.x == badXs[i]+2 && message.body.y == badYs[i]-2)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {

                newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX+1;
            badYs[count] = tempY-1;
            count++;
            badXs[count] = tempX+2;
            badYs[count] = tempY+2;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
            
        }
            //newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-1 && message.body.y == tempY+1)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
        }
          //  newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-2 && message.body.y == tempY+2)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]-1 && message.body.y == badYs[i]+1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX-1;
            badYs[count] = tempY+1;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-3 && message.body.y == tempY+3)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                   else if (verdad != true && message.body.x == badXs[i]-1 && message.body.y == badYs[i]+1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                   else if (verdad != true && message.body.x == badXs[i]-2 && message.body.y == badYs[i]+2)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX-1;
            badYs[count] = tempY+1;
            count++;
            badXs[count] = tempX-2;
            badYs[count] = tempY-2;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

          //  newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-1 && message.body.y == tempY-1)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

           // newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-2 && message.body.y == tempY-2)
            {
                for (var i = 0; i < badXs.length; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]-1 && message.body.y == badYs[i]-1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX-1;
            badYs[count] = tempY-1;
            count++;
            if (turnID > 1)
            {
                badXs[count] = tempX;
                badYs[count] = tempY;
                count++;
            }
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
        }

            //newBody.heading = "Player " + (playerNo+1%2);
            }
            else if (message.body.x == tempX-3 && message.body.y == tempY-3)
            {
                for (var i = 0; i < badXs; i++)
                {
                    if (verdad != true && message.body.x == badXs[i] && message.body.y == badYs[i])
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]-1 && message.body.y == badYs[i]-1)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    else if (verdad != true && message.body.x == badXs[i]-2 && message.body.y == badYs[i]-2)
                    {
                        verdad = true;
                        newMsg = messages['response'][4]['msg'];
                        newBody = messages['response'][4]['body'];
                    }
                    
                }
                if (verdad != true && message.body.x == initialStartX && message.body.y == initialStartY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                else if (verdad != true && message.body.x == initialEndX && message.body.y == initialEndY)
                {
                    verdad = true;
                    newMsg = messages['response'][4]['msg'];
                    newBody = messages['response'][4]['msg'];
                }
                if (verdad == false)
                {
            newMsg = messages['response'][3]['msg'];
            newBody = messages['response'][3]['body'];
            newBody.newLine.start.x = tempX;
            newBody.newLine.start.y = tempY;
            
            newBody.newLine.end.x = message.body.x;
            newBody.newLine.end.y = message.body.y;
            console.log(newBody.newLine.end.x);
            console.log(newBody.newLine.end.y);
            badXs[count] = tempX-1;
            badYs[count] = tempY-1;
            count++;
            badXs[count] = tempX-2;
            badYs[count] = tempY-2;
            count++;
            fooX = newBody.newLine.end.x;
            fooY = newBody.newLine.end.y;
                }

            //newBody.heading = "Player " + (playerNo+1%2);
            }
            
            else
            {
                //console.log("SORRIE");
                console.log(tempX);
                console.log(tempY);
                newMsg = messages['response'][4]['msg'];
            newBody = messages['response'][4]['body'];
            //newBody.heading = "Player " + (playerNo+1%2);
            }
            
        }
        if (newMsg == messages['response'][3]['msg'])
        {
            tempTurnID = turnID + 1;
            if(tempTurnID %2 == 0)
            {
                newBody.heading = "Player 2";
            }
            else{
                newBody.heading = "Player 1";
            }
        }
        
        if(tempX == initialStartX && tempY == initialStartY)
        {
            console.log("KONICA MINOLTA");
            
            initialStartX = fooX;
            initialStartY = fooY;
            console.log("NEW INITIAL START X: " + initialStartX);
            console.log("NEW INITIAL START Y: " + initialStartY);
        }
        else if (tempX == initialEndX && tempY == initialEndY)
        {
            console.log("MINOLTA KONICA");
            
            initialEndX = fooX;
            initialEndY = fooY;
            console.log("NEW INITIAL END X: " + initialEndX);
            console.log("NEW INITIAL END Y: " + initialEndY);
        }
        console.log("CURRENT INITIAL START  X VALUE: " + initialStartX);
        console.log("CURRENT INITIAL START Y VALUE: " + initialStartY);
        console.log("CURRENT INITIAL END X VALUE: " + initialEndX);
        console.log("CURRENT INITIAL END Y VALUE: " + initialEndY);
        console.log("CURRENT TURN ID: " + turnID);
        }
        else if (newMsg == "VALID_END_NODE" )
        {
            
            //newBody.heading = "Player " + (playerNo+1%2);
            playerNo = playerNo + 1;
            turnID++;
            console.log("PLAYER NUMBER: " + playerNo);
            if (playerNo % 2 == 0)
            {
                newMsg.heading = "Player 2";
            }
            else
            {
                newMsg.heading = "Player 1";
            }
            
            if (turnID > 1)
            {
                console.log("WHERE PATH STARTS X:" + initialStartX);
                console.log("WHERE PATH STARTS Y: " + initialStartY);
                console.log("WHERE PATH ENDS X:" + initialEndX);
                console.log("WHERE PATH ENDS Y: " + initialEndY);
                console.log("CHOSEN X: " + message.body.x);
                console.log("CHOSEN Y: " + message.body.y);
                console.log(message.body.x == initialStartX && message.body.y == initialStartY && turnID > 1)
                console.log(message.body.x == initialEndX && message.body.y == initialEndY && turnID > 1)
            }
            
            if (message.body.x == initialStartX && message.body.y == initialStartY && turnID > 1)
            {
                console.log("STARTED AT THE START");
               
                    newMsg = messages['response'][1]['msg'];
                    newBody  = messages['response'][1]['body'];
                    tempX = message.body.x;
                    tempY = message.body.y;
                    if (turnID%2 ==  0)
                    {
                    newBody.heading = "Player 2";
                    }
                    else{
                    newBody.heading = "Player 1";
                    }
                
            }
            else if (message.body.x == initialEndX && message.body.y == initialEndY && turnID > 1)
            {
                console.log("STARTED AT THE END");
               
                    newMsg = messages['response'][1]['msg'];
                    newBody  = messages['response'][1]['body'];
                    tempX = message.body.x;
                    tempY = message.body.y;
                    if (turnID%2 ==  0)
                    {
                    newBody.heading = "Player 2";
                    }
                    else{
                    newBody.heading = "Player 1";
                    }
                
            }
            //newBody.heading = messages['response'][]
            else if((message.body.x == newBody.newLine.end.x) && (message.body.y == newBody.newLine.end.y))
            {
                if (turnID == 2)
                {
                    initialStartX = newBody.newLine.start.x;
                    initialStartY = newBody.newLine.start.y;
                    initialEndX = newBody.newLine.end.x;
                    initialEndY = newBody.newLine.end.y;
                }
                console.log("HELLO WORLD");
                //turnID++;
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                if (turnID%2 ==  0)
                {
                    newBody.heading = "Player 2";
                }
                else{
                    newBody.heading = "Player 1";
                }
                //newBody.heading = messages['response'][1]['body'].heading[1];
                tempX = message.body.x;
                tempY = message.body.y;
                properEndX = message.body.x;
                //console.log("PROPER END X " + properEndX);
                properEndY = message.body.y;
                //console.log("PROPER END Y " + properEndY);
                
                
            }
            else if((message.body.x == newBody.newLine.start.x) && (message.body.y == newBody.newLine.start.y) && (turnID == 2))
            {
                console.log("The turn ID is 1");
                //turnID++;
                initialEndX = newBody.newLine.end.x;
                initialEndY = newBody.newLine.end.y;
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                newBody.heading = "Player 2";
                console.log(message.msg);
                tempX = message.body.x;
                tempY = message.body.y;
                console.log("NEW MESSAGE: "  + newMsg);
                properEndX = message.body.x;
                properEndY = message.body.y;
                initialStartX = message.body.x;
                initialStartY = message.body.y;
                console.log("PATH STARTING X: " + initialStartX);
                console.log("PATH STARTING Y: " + initialStartY);
                
            }
            else if((message.body.x == properEndX) && (message.body.y == properEndY) && turnID > 2)
            {
                console.log("TEMP X: " + tempX);
                console.log("TEMP Y: " + tempY);
                console.log("TRUE");   
                    newMsg = messages['response'][2]['msg'];
                    newBody = messages['response'][2]['body'];
                    
                tempX = message.body.x;
                tempY = message.body.y;
                properEndX = message.body.x;
                properEndY = message.body.y;
             
            }
            
            else
            {
                console.log("FALSE");
                newMsg = messages['response'][2]['msg'];
                newBody = messages['response'][2]['body'];
            }
        }
        else if (newMsg == "INVALID_START_NODE")
        {
            console.log("VALID START X" + fooX);
            console.log("VALID START Y" + fooY);
            if(message.body.x == fooX && message.body.y == fooY)
            {
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                tempX = fooX;
                tempY = fooY;
                
            }
            else if (message.body.x == initialStartX && message.body.y == initialStartY)
            {
                

                var verdad2 = false;
                for (var i = 0; i < badXs.length; i++)
                {
                 if(badXs[i] == properEndX && badYs[i] == properEndY && verdad2 != true)
                   {                                      
                      
                    newMsg = messages['response'][2]['msg'];
                    newBody = messages['response'][2]['body'];
                    verdad2 = true;
                   } 
                }
                console.log(badXs);
                if (verdad2 == false)
                {
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                tempX = message.body.x;
                tempY = message.body.y;
               
                }
            }
            else if (message.body.x == initialEndX && message.body.y == initialEndY)
            {
                

                
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                tempX = message.body.x;
                tempY = message.body.y;
               
                //}
            }
            else{
                console.log("BOSTON RED SOX");
                newMsg = messages['response'][2]['msg'];
                newBody = messages['response'][2]['body'];
                
            }
        }
        else if (newMsg == "INVALID_END_NODE")
        {
            if(turnID == 1)
            {
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                initialEndX = message.body.x;
                initialEndY = message.body.y;
                tempX = initialEndX;
                tempY = initialEndY;
                                               
            }
            else
            {
                console.log("TEMP X: " + tempX);
                console.log("TEMP Y: " + tempY);
                console.log("MESSAGE X: " + message.body.x);
                console.log("MESSAGE Y: " + message.body.y);
                if (message.body.x == initialStartX && message.body.y == initialStartY)
                {
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                }
                else if (message.body.x == initialEndX && message.body.y == initialEndY)
                {
                newMsg = messages['response'][1]['msg'];
                newBody = messages['response'][1]['body'];
                }
                else
                {
                newMsg = messages['response'][2]['msg'];
                newBody = messages['response'][2]['body'];
                }
        }
        }
        else if (newMsg == "GAME_OVER")
        {
            if (turnID % 2 == 1)
            {
                newBody.message = "Player 2 Wins!";
            }
            else
            {
                newBody.message = "Player 1 Wins!";
            }
        }
            
        
        else{
            newMsg = messages['response'][1]['msg'];
            newBody = messages['response'][1]['body'];
            console.log(message);
            tempX = message.body.x;
            tempY = message.body.y;
            
            
        }
    }
    console.log(newMsg);
    
        app.ports.response.send(({msg: newMsg, body: newBody}));
   
    
});


}

startGame();


